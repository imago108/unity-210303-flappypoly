﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ProcedualModel;

public class StageManager : MonoBehaviour
{
    // Start is called before the first frame update

    public int Segment = 5;
    public float Bpm = 120;

    public GameObject PrefabWall;

    void SpawnWall(int SegmentIndex, float Width)
    {
        Trapezoid wall = Instantiate(PrefabWall, new Vector3(0, 0, 0), Quaternion.identity).GetComponent<Trapezoid>();
        // Init wall
        wall.Segments = this.Segment;
        wall.SegmentIndex = SegmentIndex;
        wall.Lead = 4.0f;
        wall.Width = Width;
        wall.Bpm = this.Bpm;
    }

    void Start()
    {
        StartCoroutine(Co());
    }

    IEnumerator Co()
    {
        for (int i = 0; i < 10; i++)
        {
            SpawnWall(1, 0.25f);
            SpawnWall(0, 0.25f);
            SpawnWall(3, 0.25f);
            SpawnWall(4, 0.25f);
            yield return new WaitForSeconds(1.0f);

            SpawnWall(0, 0.5f);
            SpawnWall(2, 0.5f);
            SpawnWall(3, 0.5f);
            SpawnWall(4, 0.5f);
            yield return new WaitForSeconds(1.0f);
            
            SpawnWall(1, 0.2f);
            SpawnWall(2, 0.5f);
            SpawnWall(3, 0.2f);
            SpawnWall(4, 0.5f);
            yield return new WaitForSeconds(1.0f);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
