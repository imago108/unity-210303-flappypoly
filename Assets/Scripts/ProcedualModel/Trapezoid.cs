﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProcedualModel
{

public class Trapezoid : ProcedualModelBase
{

    // Wall Parameters
    // Lead Position
    [SerializeField, Range(0.1f, 10f)] 
    public float Lead = 1.0f;
    [SerializeField, Range(0.1f, 10f)] 
    public float Width = 0.1f;
    [SerializeField, Range(3, 12)] 
    public int Segments = 6;
    [SerializeField, Range(0, 12)] 
    public int SegmentIndex = 0;

    public float Bpm = 120;

    protected override Mesh Build()
    {
        var mesh = new Mesh();

        var vertices = new List<Vector3>();
        var normals = new List<Vector3>();
        var triangles = new List<int>();

        // Create vertices
        float angle_a = ( ( Mathf.PI * 2.0f ) / (float)Segments ) * (SegmentIndex % Segments);
        float angle_b = ( ( Mathf.PI * 2.0f ) / (float)Segments ) * ((SegmentIndex + 1) % Segments);
        Vector3 pos_a_lead = new Vector3(Mathf.Sin(angle_a), Mathf.Cos(angle_a)) * Mathf.Max(Lead, 0f);
        Vector3 pos_b_lead = new Vector3(Mathf.Sin(angle_b), Mathf.Cos(angle_b)) * Mathf.Max(Lead, 0f);
        Vector3 pos_a_end = new Vector3(Mathf.Sin(angle_a), Mathf.Cos(angle_a)) * Mathf.Max((Lead + Width), 0f);
        Vector3 pos_b_end = new Vector3(Mathf.Sin(angle_b), Mathf.Cos(angle_b)) * Mathf.Max((Lead + Width), 0f);
        vertices.Add(pos_a_lead);
        vertices.Add(pos_a_end);
        vertices.Add(pos_b_end);
        vertices.Add(pos_b_lead);

        // Create Vertex Normal
        normals.Add( new Vector3(0f, 0f, -1f) );
        normals.Add( new Vector3(0f, 0f, -1f) );
        normals.Add( new Vector3(0f, 0f, -1f) );
        normals.Add( new Vector3(0f, 0f, -1f) );

        // Create poly
        triangles.Add(0);
        triangles.Add(1);
        triangles.Add(2);
        triangles.Add(2);
        triangles.Add(3);
        triangles.Add(0);

        mesh.vertices = vertices.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = triangles.ToArray();

        mesh.RecalculateBounds();

        return mesh;
    }

    void Update()
    {
        float life = Lead + Width;
        if (life <= 0.1f)
        {
            Destroy(gameObject);
            return;
        }

        // transform.localScale += new Vector3(0.01f, 0.01f, 0f);
        Lead -= ( this.Bpm / 120.0f ) * Time.deltaTime;
        Rebuild();
    }


}

}