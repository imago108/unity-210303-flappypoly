﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProcedualModel
{

public class Cap : ProcedualModelBase
{

    // Wall Parameters
    // Lead Position
    [SerializeField, Range(0.1f, 10f)] 
    public float Radius = 1.0f;
    [SerializeField, Range(3, 12)] 
    public int Segments = 6;

    public float Bpm = 120;

    protected override Mesh Build()
    {
        var mesh = new Mesh();

        var vertices = new List<Vector3>();
        var normals = new List<Vector3>();
        var triangles = new List<int>();

        // First, Create origin vertex
        vertices.Add(new Vector3(0f, 0f, 0f));
        normals.Add( new Vector3(0f, 0f, -1f) );

        // Create vertices each segments
        for (int i = 0; i < Segments; i++)
        {
            float angle = ( ( Mathf.PI * 2.0f ) / (float)Segments ) * (float)i;
            Vector3 pos = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0f);
            // affects radius
            pos *= Radius;
            // apply vertex and vertex normal
            vertices.Add(pos);
            normals.Add( new Vector3(0f, 0f, -1f) );
        }

        // Create faces
        for (int i = 0; i < Segments; i++)
        {
            // 0 -> i+1 -> i+2
            int a = 0;
            int b = i + 1;
            int c = i + 2;
            if (c > Segments)
                c = 1;
            triangles.Add(a);
            triangles.Add(b);
            triangles.Add(c);
        }

        mesh.vertices = vertices.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = triangles.ToArray();

        mesh.RecalculateBounds();

        return mesh;
    }

    void Update()
    {
        float life = Radius;
        if (life <= 0.1f)
        {
            Destroy(gameObject);
            return;
        }

        // transform.localScale += new Vector3(0.01f, 0.01f, 0f);
        Radius -= ( this.Bpm / 120.0f ) * Time.deltaTime;
        Rebuild();
    }


}

}