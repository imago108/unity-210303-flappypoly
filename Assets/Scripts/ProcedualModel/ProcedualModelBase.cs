﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProcedualModel
{

[RequireComponent (typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public abstract class ProcedualModelBase : MonoBehaviour
{

    // Components
    private MeshFilter _filter;
    private MeshRenderer _renderer;
    private MeshCollider _collider;

    // Properties
    public MeshFilter Filter {
        get {
            if (_filter == null)
                _filter = GetComponent<MeshFilter>();
            return _filter;
        }
    }

    public MeshRenderer Renderer {
        get {
            if (_renderer == null)
                _renderer = GetComponent<MeshRenderer>();
            return _renderer;
        }
    }

    public MeshCollider Collider {
        get {
            if (_collider == null)
            {
                _collider = GetComponent<MeshCollider>();
            }
            return _collider;
        }
    }

    protected virtual void Start () {
		Rebuild();
	}

    public void Rebuild() {
        // Destroy MeshFilter
        if(Filter.sharedMesh != null) {
            if(Application.isPlaying) {
                Destroy(Filter.sharedMesh);
            } else {
                DestroyImmediate(Filter.sharedMesh);
            }
        }
        // Destroy MeshCollider
        if(Collider.sharedMesh != null) {
            if(Application.isPlaying) {
                Destroy(Collider.sharedMesh);
            } else {
                DestroyImmediate(Collider.sharedMesh);
            }
        }
        // Build new mesh
        var newMesh = Build();
		Filter.sharedMesh = newMesh;
        Collider.sharedMesh = newMesh;

        // Renderer.sharedMaterial = LoadMaterial(materialType);
    }

    protected abstract Mesh Build();


    
    
}

}
