﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float Speed = 0.01f;

    private float positionAlpha = 0.0f;
    private float maxSpeed = 1.0f;

    private int currentIndex = 0;


    // Start is called before the first frame update
    void Start()
    {
        transform.position = GetPlayerPosition(currentIndex, 5);
    }

    bool IsMoving()
    {
        return false;
    }

    Vector3 GetPlayerPosition(int SegmentIndex, int Segment)
    {
        int segIndex = SegmentIndex % Segment;

        float alpha = (float)segIndex / (float)Segment;

        alpha = Mathf.Floor(alpha * (float)Segment);
        alpha /= (float)Segment;

        float pi2 = Mathf.PI * 2f;
        alpha *= pi2;
        alpha += ( pi2 / (float)Segment ) / 2.0f;
        
        float x = Mathf.Sin(alpha);
        float y = Mathf.Cos(alpha);

        return new Vector3(x, y, 0f);
    }

    /// <summary>
    /// Move to right segment
    /// </summary>
    void MoveRightSegment()
    {

    }

    /// <summary>
    /// Move to left segment
    /// </summary>
    void MoveLeftSegment()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("a"))
        {
            currentIndex = currentIndex - 1;
            transform.position = GetPlayerPosition(currentIndex, 5);
        }
        if (Input.GetKeyDown("d"))
        {
            currentIndex = currentIndex + 1;
            transform.position = GetPlayerPosition(currentIndex, 5);
        }

    }
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("GAME OVER");
    }
}
